## About

Allows user to generate public getters for private (serialized) fields in Unity.

## Usage

Write a partial class, mark your private/protected fields with `[GenPublicGetter]` attribute.

```csharp

namespace MyNamespace
{

    public partial class MyClass
    {
    
        [GenPublicGetter]
        private int _myValue = 0;
        
    }
}
```

After build, a new file will be generated with public getter for your field.

```csharp
namespace MyNamespace
{
    public partial class MyClass
    {
    
		public int MyValue
			=> _myValue;

    }
}
```

If you using assembly definitions, you need to explicitly reference `Valhalla.PublicGettersGenerator` assembly.
