## [1.0.2] – 2023-10-24

### Changed
- Dependency updated for Windows support (again)


## [1.0.1] – 2023-10-24

### Changed
- Dependency updated for Windows support


## [1.0.0] – 2023-07-25

### Changed
- Project structure refactored
- Namespaces renamed


## [0.1.4] – 2023-06-16

### Changed
- ExtendedSourceGenerators updated to v1.1.1
- Generation logic refactored


## [0.1.3] – 2023-06-16

### Changed
- ExtendedSourceGenerators updated to v1.1.0

## [0.1.2] – 2023-04-18

### Fixed
- `static` field don't break other field anymore
- Getters for fields with nested class type are possible now


## [0.1.1] – 2023-04-18

### Added
- Multiple classes support
- Changelogs

### Fixed
- Nested class are now ignored properly


## [0.1.0] – 2023-03-09

### Added
- Generation of public getters
